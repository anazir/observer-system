<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(2);

$getCompaniesAndAdmins = sqlsrv_query(
  $conn,
  "{CALL P_get_company_info(?)}",
  [$_GET['search']]
);
?>

  <html>
<head>
  <title>Select Company to update</title>
  <link rel="stylesheet" href="../bulma.css">
</head>
<body>
  <div class="container">
    <h1 class="title">Select Company to update</h1>

    <div class="field">
      <a class="button is-text" href="./">Home</a>
    </div>

    <form method="get">
      <label class="label">Search by Company or Admin Name or Username</label>
      <div class="field has-addons is-fullwidth">
        <div class="control is-fullwidth">
          <input class="input is-fullwidth" type="text" name="search">
        </div>
        <div class="control">
          <button class="button is-primary">
            Search
          </a>
        </div>
      </div>
    </form>


    <h3 class="title">Results</h3>
<?php 
echo ("<table class='table is-fullwidth'><tr >");

foreach( sqlsrv_field_metadata($getCompaniesAndAdmins) as $fieldMetadata ) {
  echo ("<th class='is-uppercase'>");
  echo $fieldMetadata["Name"];
  echo ("</th>");
}
echo ("<th></th></tr>");

while ($row = sqlsrv_fetch_array($getCompaniesAndAdmins, SQLSRV_FETCH_ASSOC)) {
  echo ("<tr>");
  foreach($row as $col){
    echo ("<td>");
    if(is_a($col, 'DateTime')) {
      $col = $col->format('Y-m-d');
    }
    echo (is_null($col) ? "Null" : $col);
    echo ("</td>");
  }
  echo ("<td><form method='get' action='./update-companies.php'><button class='button is-primary' type='submit' name='oldID' value='".$row['registration_id']."'>Select</button></form></td>");
  echo ("</tr>");
}
echo ("</table>");
?>
  </div>
</body>
</html>
