<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(2);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $sql = $_POST['sql'];

  $query = sqlsrv_query(
    $conn,
    $sql ?? '',
    []
  );

  if( $query === false  ) {
    echo print_r( sqlsrv_errors(), true );
  }
}
?>

<html>
<head>
  <title>SQL runner</title>
  <link rel="stylesheet" href="../bulma.css">
</head>
<body>
  <div class="container">
    <h1 class="title">SQL runner</h1>

    <h3 class="subtitle">Run sql</h3>

    <form method="POST">
      <div class="field">
        <textarea class="textarea" cols="200" rows="25" name="sql"><?php echo $_POST['sql'] ?></textarea>
      </div>
      <button class="button is-primary" type="submit">Submit</button>
    </form>

    <h3 class="title">Results</h3>
    <?php PrintMultiResultSet($query) ?>
  </div>
</body>
</html>
