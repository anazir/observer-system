<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(2);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $registrationNum = $_POST['registrationNum'];
  $compName = $_POST['compName'];
  $idNum = $_POST['idNum'];
  $name = $_POST['name'];
  $username = $_POST['username'];
  $password = $_POST['password'];
  $sex = $_POST['sex'];
  $jobTitle = $_POST['jobTitle'];
  $birthdate = $_POST['birthdate'];

  if (empty($registrationNum)) echo "Registration Number is empty!<br/>";
  if (empty($compName)) echo "Company Name is empty!<br/>";
  if (empty($idNum)) echo "Id Number is empty!<br/>";
  if (empty($name)) echo "Name is empty!<br/>";
  if (empty($username)) echo "Username is empty!<br/>";
  if (empty($password)) echo "Password is empty!<br/>";
  if (empty($sex) && $sex != 0) echo "Sex is empty!<br/>";
  if (empty($jobTitle)) echo "Job Title is empty!<br/>";
  if (empty($birthdate)) echo "Birthdate is empty!<br/>";

  if (!(
    empty($registrationNum) ||
    empty($compName) ||
    empty($idNum) ||
    empty($name) ||
    empty($username) ||
    empty($password) ||
    empty($sex) && $sex != 0 ||
    empty($jobTitle) ||
    empty($birthdate)
  )) {
    $query = sqlsrv_query(
      $conn,
      "{CALL P_create_company(?, ?, ?, ?, ?, ?, ?, ?, ?)}",
      [$registrationNum, $compName, $idNum, $name, $username, $password, $sex, $jobTitle, $birthdate]
    );

    if( $query === false   ) {
      echo print_r( sqlsrv_errors(), true  );
    }
  }
}

$getCompaniesAndAdmins = sqlsrv_query(
  $conn,
  "EXEC P_get_company_info",
    []
  );
?>

<html>
<head>
  <title>Create new Company</title>
  <link rel="stylesheet" href="../bulma.css">
</head>
<body>
  <div class="container">
    <h1 class="title">Create new Company</h1>

    <a class="button is-text"href="./">Home</a>

    <section class="box">
      <div class="section">
        <form method="post">
          <h3 class="title is-4">Company</h3>
          <div class="field">
            <label class="label">Registration Number</label>
            <input class="input" type="number" name="registrationNum"><br>
          </div>
          <div class="field">
            <label class="label">Name</label>
            <input class="input" type="text" name="compName"><br>
          </div>
          <h3 class="title is-4">Admin</h3>
          <div class="field">
            <label class="label">ID Number</label>
            <input class="input" type="number" name="idNum"><br>
          </div>
          <div class="field">
            <label class="label">Name</label>
            <input class="input" type="text" name="name"><br>
          </div>
          <div class="field">
            <label class="label">Username</label>
            <input class="input" type="text" name="username"><br>
          </div>
          <div class="field">
            <label class="label">Password</label>
            <input class="input" type="password" name="password"><br>
          </div>
          <div class="field">
            <label class="label">Sex</label>
            <div class="select is-fullwidth">
              <select name="sex">
                <option value='0'>Male</option>
                <option value='1'>Female</option>
              </select>
            </div>
          </div>
          <br>
          <div class="field">
            <label class="label">Job Title</label>
            <input class="input" type="text" name="jobTitle"><br>
          </div>
          <div class="field">
            <label class="label">Birth Date</label>
            <input class="input" type="date" name="birthdate"><br>
          </div>
          <input class="button is-primary" type="submit" name="connect"> 
        </form>
      </div>
    </section>

    <h3 class="title">Results</h3>
    <?php PrintResultSet($getCompaniesAndAdmins) ?>
  </div>
</body>
</html>
