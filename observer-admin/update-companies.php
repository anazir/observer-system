<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(2);

$oldID = $_GET['oldID'];

$getAdmin = sqlsrv_query(
  $conn,
  "{CALL P_get_company_manager(?)}",
  [$oldID]
);

$admin = sqlsrv_fetch_array($getAdmin, SQLSRV_FETCH_ASSOC)['id_number'];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $registrationNum = $_POST['registrationNum'] ?: null;
  $compName = $_POST['compName'] ?: null;
  $newManager = $_POST['newManager'];
  $name = $_POST['name'] ?: null;
  $username = $_POST['username'] ?: null;
  $password = $_POST['password'] ?: null;
  $sex = [
    '' => null,
    '1' => 1,
    '0' => 0
  ][$_POST['sex']];
  $jobTitle = $_POST['jobTitle'] ?: null;
  $birthdate = $_POST['birthdate'] ?: null;

  if (empty($oldID) && $oldID != 0) echo "Old registration number is empty!<br/>";

  if (!empty($oldID) || $oldID == 0) {
    $query = sqlsrv_query(
      $conn,
      "{CALL P_modify_company(?, ?, ?, ?)}",
      [$oldID, $registrationNum, $compName, $newManager]
    );

    if( $query === false   ) {
      echo print_r( sqlsrv_errors(), true  );
    }

    $query = sqlsrv_query(
      $conn,
      "{CALL P_modify_manager(?, ?, ?, ?, ?, ?, ?)}",
      [$admin, $name, $sex, $jobTitle, $birthdate, $username, $password]
    );

    if( $query === false   ) {
      echo print_r( sqlsrv_errors(), true  );
    }

    // Navigate change old ID to new ID
    $get = $_GET;
    $get['oldID'] = $registrationNum ?? $oldID; 
    $query_result = http_build_query($get);
    $finalUrl = $_SERVER['PHP_SELF'] .'?'. $query_result;
    header('Location: '.$finalUrl);
  }
}

$getAdmins = sqlsrv_query(
  $conn,
  "{CALL P_get_simple_user_info(?)}",
  [$oldID]
);

$getCompaniesAndAdmins = sqlsrv_query(
  $conn,
  "EXEC P_get_company_info",
    []
  );
?>

  <html>
<head>
  <title>Update Company</title>
  <link rel="stylesheet" href="../bulma.css">
</head>
<body>
<div class="container">
  <h1 class="title">Update Company with ID <?php echo $_GET['oldID'] ?></h1>
    <a class="button is-text" href="./">Home</a>

    <div class="box">
      <h3 class="title is-4">Company</h3>

      <?php if(isset($_GET['admin']) && $_GET['admin'] == 'true') { ?>
        <form id="cancel-admin-change" method="get">
            <input name="oldID" value="<?php echo $_GET['oldID'] ?>" type="hidden">
            <input name="admin" value="false" type="hidden">
            <button class="button" form="cancel-admin-change" type="submit">Leave the admin as is</button>
        </form>
      <?php } else { ?>
        <form method="get">
          <input name="oldID" value="<?php echo $_GET['oldID'] ?>" type="hidden">
          <input name="admin" value="true" type="hidden">
          <button class="button" type="submit">Make another user the admin</button>
        </form>
      <?php } ?>


      <form method="post">
        <input name="oldID" value="<?php echo $_GET['oldID'] ?>" type="hidden">
        <div class="field">
          <label class="label">New Registration Number</label>
          <input class="input" type="number" name="registrationNum"><br>
        </div>
        <div class="field">
          <label class="label">New Name</label>
          <input class="input" type="text" name="compName"><br>
        </div>
        <?php if(isset($_GET['admin']) && $_GET['admin'] == 'true') { ?>
          <div class="field">
            <label class="label">New Admin</label>
            <div class="select is-fullwidth">
              <select name="newManager">
                <?php
                while ($row = sqlsrv_fetch_array($getAdmins, SQLSRV_FETCH_ASSOC)) {
                  echo ("<option value='".$row['id_number']."'>".$row['name']."</option>");
                }
                ?>
              </select><br>
            </div>
          </div>

        <?php } else { ?>
          <h3 class="title is-4">Admin</h3>
          <div class="field">
            <label class="label">New Name</label>
          <input class="input" type="text" name="name"><br>
          </div>
          <div class="field">
            <label class="label">New Username</label>
          <input class="input" type="text" name="username"><br>
          </div>
          <div class="field">
            <label class="label">New Password</label>
          <input class="input" type="password" name="password"><br>
          </div>
          <div class="field">
            <label class="label">New Sex</label>
            <div class="select is-fullwidth">
              <select name="sex">
                <option value=''>Leave as is</option>
                <option value='0'>Male</option>
                <option value='1'>Female</option>
              </select>
            </div>
          </div>
          <div class="field">
            <label class="label">New Job Title</label>
          <input class="input" type="text" name="jobTitle"><br>
          </div>
          <div class="field">
            <label class="label">New Birth Date</label>
          <input class="input" type="date" name="birthdate"><br>
          </div>
        <?php } ?>
      <input class="button is-primary" type="submit" name="connect">
    </form>
  </div>

  <?php PrintResultSet($getCompaniesAndAdmins) ?>
</div>
</body>
</html>
