<?php
include_once '../helpers/functions.php';
authenticate(2);
?>

<html>
<head>
  <title>Observer Admin</title>
  <link rel="stylesheet" href="../bulma.css">
</head>
<body>
  <div class="hero is-primary">
    <div class="hero-body">
      <div class="level">
        <h1 class="title">Observer Admin</h1>
        <a class="button is-primary is-medium" href="../">
          Login Page
        </a>
      </div>
    </div>
  </div>

  <section class="section">
    <a class="button is-text" href="./sql-interface.php">Run sql statements</a><br>
    <a class="button is-text" href="./create-company.php">Create Company</a><br>
    <a class="button is-text" href="./select-companies.php">Update Companies</a>
  </section>

  <section class="section">
    <h2 class="title">Company Admin Interfaces</h2>
    <a class="button is-text" href="../company-admin/select-user.php">View Users</a><br>
  </section>

  <section class="section">
    <h2 class="title">Simple User Interfaces</h2>
    <a class="button is-text" href="../simple-user/manage-questions.php">Manage Questions</a><br>
    <a class="button is-text" href="../simple-user/manage-questionnaires.php">Manage Questionnaires</a><br>
    <a class="button is-text" href="../simple-user/company-questionaires.php">Company Questionaires</a><br>
    <a class="button is-text" href="../simple-user/popular-questions.php">Most popular question</a><br>
    <a class="button is-text" href="../simple-user/questionaires-and-question-number.php">Questionaires and question number</a><br>
    <a class="button is-text" href="../simple-user/average-question-count.php">Average question count</a><br>
    <a class="button is-text" href="../simple-user/big-questionaires.php">Big questionaires</a><br>
    <a class="button is-text" href="../simple-user/small-questionaires.php">Small questionaires</a><br>
    <a class="button is-text" href="../simple-user/questionaires-with-common-questions.php">Questionaires with the same questions</a><br>
    <a class="button is-text" href="../simple-user/questionaires-wich-contain-questions.php">Questionaires with common questions</a><br>
    <a class="button is-text" href="../simple-user/least-used-questions.php">Least used questions</a><br>
    <a class="button is-text" href="../simple-user/everpresent-questions.php">Everpresent questions</a><br>
    <a class="button is-text" href="../simple-user/subsequent-versions-of-questionaire.php">Subsequent versions of questionaire</a><br>
  </section>
</body>
</html>
