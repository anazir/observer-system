<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(0);

$errors = '';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $userID = $_SESSION['id'];
  $companyID = $_SESSION['company_id'];
  $questionID = $_POST['questionID'];
  $questionaireID = $_POST['questionaireID'];

  if (empty($userID)) $errors = $errors. "User ID is empty!<br/>";
  if (empty($companyID)) $errors = $errors. "Company is empty!<br/>";
  if (empty($questionID)) $errors = $errors. "Question is empty!<br/>";
  if (empty($questionaireID)) $errors = $errors. "Questionaire is empty!<br/>";

  if (!empty($userID) && !empty($companyID) && !empty($questionID) && !empty($questionaireID)) {
    $query = sqlsrv_query(
      $conn,
      "{CALL P_remove_question(?, ?, ?, ?)}",
      [$userID, $companyID, $questionaireID, $questionID]
    );

    if( $query === false   ) {
      $errors = $errors.sqlsrv_errors()[0]['message'];
    }
  }
}

header("Location: ./update-questionnaire.php?questionaireID=".$questionaireID."&errors=".nl2br($errors));
?>
