<?php
include_once '../helpers/functions.php';
authenticate(0);
forbid(1);
forbid(2);
?>

<html>
<head>
  <title>Simple User</title>
  <link rel="stylesheet" href="../bulma.css">
</head>
<body>
  <div class="hero is-warning">
    <div class="hero-body">
      <div class="level">
        <h1 class="title">Simple User</h1>
        <a class="button is-warning is-medium" href="../">
          Login Page
        </a>
      </div>
    </div>
  </div>

  <section class="section">
    <a class="button is-text" href="./manage-questions.php">Manage Questions</a><br>
    <a class="button is-text" href="./manage-questionnaires.php">Manage Questionnaires</a><br>
    <a class="button is-text" href="./company-questionaires.php">Company Questionaires</a><br>
    <a class="button is-text" href="./popular-questions.php">Most popular question</a><br>
    <a class="button is-text" href="./questionaires-and-question-number.php">Questionaires and question number</a><br>
    <a class="button is-text" href="./average-question-count.php">Average question count</a><br>
    <a class="button is-text" href="./big-questionaires.php">Big questionaires</a><br>
    <a class="button is-text" href="./small-questionaires.php">Small questionaires</a><br>
    <a class="button is-text" href="./questionaires-with-common-questions.php">Questionaires with the same questions</a><br>
    <a class="button is-text" href="./questionaires-wich-contain-questions.php">Questionaires with common questions</a><br>
    <a class="button is-text" href="./least-used-questions.php">Least used questions</a><br>
    <a class="button is-text" href="./everpresent-questions.php">Everpresent questions</a><br>
    <a class="button is-text" href="./subsequent-versions-of-questionaire.php">Subsequent versions of questionaire</a><br>
  </section>
</body>
</html>
