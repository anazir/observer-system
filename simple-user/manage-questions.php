<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(0);

echo $_GET['errors'];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $questionID = $_POST['questionID'] ?: null;
  $companyID = $_SESSION['company_id'];
  $userID = $_SESSION['id'];

  if (empty($questionID) && $questionID != 0) echo "Question id is empty!<br/>";
  if (empty($companyID) && $companyID != 0) echo "Company id is empty!<br/>";
  if (empty($userID) && $userID != 0) echo "User id is empty!<br/>";

  if ((!empty($questionID) || $questionID == 0) && (!empty($companyID) || $companyID != 0) && (!empty($userID) || $userID != 0)) {
    $query = sqlsrv_query(
      $conn,
      "{CALL P_delete_question(?, ?, ?)}",
      [$userID, $companyID, $questionID]
    );

    if( $query === false   ) {
      echo print_r( sqlsrv_errors(), true  );
    }
  }
}

$getQuestions = sqlsrv_query(
  $conn,
  "{CALL P_get_question_info(?)}",
  [$_SESSION['company_id'] ?? $_GET['companyID']]
);
if( $getQuestions === false   ) {
  echo print_r( sqlsrv_errors(), true  );
}

$color = ['warning', 'info', 'primary'][$_SESSION['user_type']];
?>

<html>
<head>
  <title>Manage Questions</title>
  <link rel="stylesheet" href="../bulma.css">
</head>
<body>
  <div class="container">
    <h1 class="title">Manage Questions</h1>

    <div class="block">
      <a class="button is-text"href="./">Home</a>
    </div>

    <?php if($_SESSION['company_id']) { ?>
    <p class="notification is-<?php echo $color ?>">To add limits/bounds or choices, edit question</p>
      <h2 class="title is-4">Add question</h2>
      <form method="post" action="./create-question.php">
        <div class="field">
          <label class="label">Description</label>
          <textarea class="textarea" name="description"></textarea><br>
        </div>
        <div class="field">
          <label class="label">Text</label>
          <textarea class="textarea" name="text"></textarea><br>
        </div>
        <div class="field">
          <label class="label">Type</label>
          <div class="select is-fullwidth">
            <select name="type">
              <option value='0'>Text</option>
              <option value='1'>Multiple Choice with single answer</option>
              <option value='2'>Multiple Choice with multiple answers</option>
              <option value='3'>Number without limits</option>
              <option value='4'>Number with limits</option>
            </select>
          </div>
        </div>
        <br>
        <input class="button is-<?php echo $color ?>" type="submit" name="connect"> 
      </form>
    <?php } else { ?>
      <form method="get">
        <div class="field">
          <label class="label">Company ID</label>
          <input class="input" value="<?php echo $_GET['companyID'] ?>" type="number" name="companyID">
        </div>
        <input class="button is-primary" type="submit">
      </form>
    <?php } ?>

<?php 
echo ("<table class='table is-fullwidth'><tr >");

foreach( sqlsrv_field_metadata($getQuestions) as $fieldMetadata ) {
  echo ("<th class='is-uppercase'>");
  echo $fieldMetadata["Name"];
  echo ("</th>");
}
echo ("<th><th></tr>");

while ($row = sqlsrv_fetch_array($getQuestions, SQLSRV_FETCH_ASSOC)) {
  echo ("<tr>");
  foreach($row as $col){
    echo ("<td>");
    if(is_a($col, 'DateTime')) {
      $col = $col->format('Y-m-d');
    }
    echo (is_null($col) ? "" : $col);
    echo ("</td>");
  }

  if($_SESSION['company_id']) {
    echo ("<td><form method='get' action='./update-question.php'><button class='button is-".$color."' type='submit' name='questionID' value='".$row['question_id']."'>Edit</button></form></td>");
    echo ("<td><form method='post'><button class='button is-danger' type='submit' name='questionID' value='".$row['question_id']."'>Delete</button></form></td>");
  }

  echo ("</tr>");
}
echo ("</table>");
?>
  </div>
</body>
</html>
