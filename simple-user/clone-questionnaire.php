<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(0);

$errors = '';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $companyID = $_SESSION['company_id'];
  $userID = $_SESSION['id'];
  $questionaireID = $_POST['questionaireID'];

  if (empty($questionaireID)) $errors = $errors. "Questionaire ID is empty!<br/>";

  if (!empty($questionaireID)) {
    $query = sqlsrv_query(
      $conn,
      "{CALL P_clone_questionaire(?, ?, ?)}",
      [$companyID, $userID, $questionaireID]
    );

    if( $query === false   ) {
      $errors = $errors.sqlsrv_errors()[0]['message'];
    }
  }
}

header("Location: ./manage-questionnaires.php?errors=".nl2br($errors));
?>
