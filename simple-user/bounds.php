<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(0);

$errors = '';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $userID = $_SESSION['id'];
  $companyID = $_SESSION['company_id'];
  $questionID = $_POST['questionID'];
  $shouldEdit = $_POST['edit'] == 'true';
  $min = $_POST['min'];
  $max = $_POST['max'];
  $areBoundsValid = (!$shouldEdit && !myEmpty($min) && !myEmpty($max)) || $shouldEdit;

  if (empty($userID)) $errors = $errors. "User ID is empty!<br/>";
  if (empty($companyID)) $errors = $errors. "Company is empty!<br/>";
  if (!$areBoundsValid) $errors = $errors. "One of the bounds is empty!<br/>";

  if (!empty($userID) && !empty($companyID) && $areBoundsValid) {
    $query = sqlsrv_query(
      $conn,
      $shouldEdit ? "{CALL P_modify_bounds(?, ?, ?, ?, ?)}" : "{CALL P_add_bounds(?, ?, ?, ?, ?)}",
      [$userID, $companyID, $questionID, $min, $max]
    );

    if( $query === false   ) {
      $errors = $errors.sqlsrv_errors()[0]['message'];
    }
  }
}

header("Location: ./update-question.php?questionID=".$questionID."&errors=".nl2br($errors));
?>
