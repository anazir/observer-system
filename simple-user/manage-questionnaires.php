<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(0);

echo $_GET['errors'];
$search = $_GET['search'];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $title = $_POST['title'];
  $userID = $_SESSION['id'];

  if (empty($title)) echo "title is empty!<br/>";
  if (empty($userID) && $userID != 0) echo "User id is empty!<br/>";

  if (!empty($title) && (!empty($userID) || $userID != 0)) {
    $query = sqlsrv_query(
      $conn,
      "{CALL P_create_questionaire(?, ?)}",
      [$userID, $title]
    );

    if( $query === false   ) {
      echo print_r( sqlsrv_errors(), true  );
    }
  }
}

$getQuestionnaires = sqlsrv_query(
  $conn,
  "{CALL P_get_questionaires(?, ?)}",
  [$_SESSION['company_id'] ?? $_GET['companyID'], $_GET['text'] ?? null]
);
if( $getQuestionnaires === false   ) {
  echo print_r( sqlsrv_errors(), true  );
}

$color = ['warning', 'info', 'primary'][$_SESSION['user_type']];
?>

  <html>
<head>
  <title>Manage Questionnaires</title>
  <link rel="stylesheet" href="../bulma.css">
</head>
<body>
  <div class="container">
    <h1 class="title">Manage Questionnaires</h1>

    <a class="button is-text" href="./">Home</a>

    <?php if($_SESSION['company_id']) { ?>
      <div class="box">
        <h2 class="title is-4">Create questionnaire</h2>
        <form method="post">
          <div class="field">
            <label class="label">Title</label>
            <input class="input" type="text" name="title"><br>
          </div>
          <input class="button is-<?php echo $color ?>" type="submit" name="connect">
        </form>
      </div>
    <?php } ?>

    <h2 class="title is-4">Search</h2>
    <form method="get">
      <?php if(!$_SESSION['company_id']) { ?>
        <div class="field">
          <label class="label">Company ID</label>
          <input class="input" value="<?php echo $_GET['companyID'] ?>" type="number" name="companyID"><br>
        </div>
      <?php } ?>
      <div class="field">
        <label class="label">Title</label>
        <input class="input" type="text" name="text"><br>
      </div>
      <input class="button is-<?php echo $color ?>" type="submit" name="connect">
    </form>

    <?php 
      echo ("<table class='table is-fullwidth'><tr >");

      foreach( sqlsrv_field_metadata($getQuestionnaires) as $fieldMetadata ) {
        echo ("<th class='is-uppercase'>");
        echo $fieldMetadata["Name"];
        echo ("</th>");
      }
      echo ("<th><th></tr>");

      while ($row = sqlsrv_fetch_array($getQuestionnaires, SQLSRV_FETCH_ASSOC)) {
        echo ("<tr>");
        foreach($row as $col){
          echo ("<td>");
          if(is_a($col, 'DateTime')) {
            $col = $col->format('Y-m-d');
          }
          echo (is_null($col) ? "" : $col);
          echo ("</td>");
        }

        if($_SESSION['company_id']) {
          if($row['isComplete'] == '0') {
            if($row['creator_id'] == $_SESSION['id']) {
              echo ("<td><form style='margin-bottom:0' method='get' action='./update-questionnaire.php'><button class='button' type='submit' name='questionaireID' value='".$row['questionaire_id']."'>Edit</button></form></td>");
            } else {
              echo ('<td></td>');
            }
            echo ('<td></td>');
          } else {
            echo ('<td></td>');
            echo ("<td><form style='margin-bottom:0' method='post' action='./clone-questionnaire.php'><button class='button is-".$color."' type='submit' name='questionaireID' value='".$row['questionaire_id']."'>Clone</button></form></td>");
          }
        }
        echo ("</tr>");
      }
      echo ("</table>");
    ?>
  </div>
</body>
</html>
