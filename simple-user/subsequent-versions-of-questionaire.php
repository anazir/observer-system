<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(0);

$getQuery = sqlsrv_query(
  $conn,
  "EXEC sp_questionares_inheritance_versions ?",
    [$_GET['questionaire']]
  );

$color = ['warning', 'info', 'primary'][$_SESSION['user_type']];
?>

<html>
<head>
  <title>Subsequent versions of questionaire</title>
  <link rel="stylesheet" href="../bulma.css">
</head>
<body>
  <div class="container">
    <h1 class="title">Subsequent versions of questionaire</h1>

    <a class="button is-text" href="./">Home</a>
    <form method="get">
      <div class="field">
        <label class="label">Questionaire ID</label>
        <input class="input" name="questionaire">
      </div>
      <input class="button is-<?php echo $color ?>" type="submit">
    </form>

    <?php if(isset($_GET['questionaire'])) { ?>
      Questionaire given: <?php echo $_GET['questionaire'] ?>
    <?php } ?>
    <?php PrintResultSet($getQuery) ?>
  </div>
</body>
</html>
