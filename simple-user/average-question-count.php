<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(0);

$getQuery = sqlsrv_query(
  $conn,
  "EXEC sp_question_average_in_questionare ?",
    [$_SESSION['company_id'] ?? $_GET['companyID']]
  );
?>

<html>
<head>
  <title>Average number of questions in Questionaires</title>
  <link rel="stylesheet" href="../bulma.css">
</head>
<body>
  <div class="container">
    <h1 class="title">Average number of questions in Questionaires</h1>

    <a class="button is-text" href="./">Home</a>
  <?php if(!$_SESSION['company_id']) { ?>
    <form method="get">
      <div class="field">
        <label class="label">Company ID</label>
          <input class="input" value="<?php echo $_GET['companyID'] ?>" type="number" name="companyID">
      </div>
      <input class="button is-primary" type="submit">
    </form>
  <?php } ?>

    <?php PrintResultSet($getQuery) ?>
  </div>
</body>
</html>
