<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(0);

$errors = '';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $userID = $_SESSION['id'];
  $companyID = $_SESSION['company_id'];
  $questionID = $_POST['questionID'];
  $choiceID = $_POST['choiceID'];

  if (empty($userID)) $errors = $errors. "User ID is empty!<br/>";
  if (empty($companyID)) $errors = $errors. "Company is empty!<br/>";
  if (empty($choiceID)) $errors = $errors. "Choice is empty!<br/>";

  if (!empty($userID) && !empty($companyID) && !empty($choiceID)) {
    $query = sqlsrv_query(
      $conn,
      "{CALL P_remove_choice(?, ?, ?, ?)}",
      [$userID, $questionID, $companyID, $choiceID]
    );

    if( $query === false   ) {
      $errors = $errors.sqlsrv_errors()[0]['message'];
    }
  }
}

header("Location: ./update-question.php?questionID=".$questionID."&errors=".nl2br($errors));
?>
