<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(0);
forbid(2);

echo $_GET['errors'];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $question = $_POST['question'];
  $oldQuestion = $_POST['oldID'];
  $userID = $_SESSION['id'];
  $companyID = $_SESSION['company_id'];
  $questionaireID = $_POST['questionaireID'];
  echo var_dump($questionaireID);

  if (empty($question)) echo "Question is empty!<br/>";

  if (!empty($question)) {
    $query = sqlsrv_query(
      $conn,
      "{CALL P_add_question(?, ?, ?, ?)}",
      [$userID, $companyID, $questionaireID, $question]
    );

    if( $query === false   ) {
      echo print_r( sqlsrv_errors(), true  );
    } else {
      $query = sqlsrv_query(
        $conn,
        "{CALL P_remove_question(?, ?, ?, ?)}",
        [$userID, $companyID, $questionaireID, $oldQuestion]
      );

      if( $query === false   ) {
        echo print_r( sqlsrv_errors(), true  );
      }
    }
  }
}

$getQuestions = sqlsrv_query(
  $conn,
  "{CALL P_get_questionaire_questions(?, ?)}",
  [$_SESSION['company_id'], $_GET['questionaireID']]
);

if( $getQuestions === false   ) {
  echo print_r( sqlsrv_errors(), true  );
}

$getCompanyQuestions = sqlsrv_query(
  $conn,
  "{CALL P_get_question_info(?)}",
  [$_SESSION['company_id']]
);

if( $getCompanyQuestions === false   ) {
  echo print_r( sqlsrv_errors(), true  );
}

$questionSelect = '<div class="select"><select name="question">';
while ($row = sqlsrv_fetch_array($getCompanyQuestions, SQLSRV_FETCH_ASSOC)) {
  $questionSelect = $questionSelect . ("<option value='".$row['question_id']."'>".$row['description'].'|'.$row['text']."</option>");
}
$questionSelect = $questionSelect . '</select></div>';

$color = ['warning', 'info', 'primary'][$_SESSION['user_type']];
?>

<html>
<head>
  <title>Update Questionnaire</title>
  <link rel="stylesheet" href="../bulma.css">
</head>
<body>
  <div class="container">
    <h1 class="title">Update Questionnaire with ID: <?php echo $_GET['questionaireID'] ?></h1>

    <div class="block">
      <a class="button is-text" href="./">Home</a>
    </div>

    <h2 class="title is-4">Add question</h2>
    <form method="post">
      <input type='hidden' name='questionaireID' value='<?php echo $_GET['questionaireID'] ?>'>
      <div class="field">
        <label class="label">New Question</label>
        <?php echo $questionSelect ?>
      </div>
      <input class="button is-<?php echo $color ?>" type="submit" name="connect">
    </form>

    <h2 class="title is-4">Complete Questionaire</h2>
    <form method="post" action="./complete-questionnaire.php">
      <button class="button is-<?php echo $color ?>" type="submit" name="questionaireID" value="<?php echo $_GET['questionaireID'] ?>">Complete Questionaire</button>
    </form>

<p class="title">Results</p>
<?php 
echo ("<table class='table is-fullwidth'><tr >");

foreach( sqlsrv_field_metadata($getQuestions) as $fieldMetadata ) {
  echo ("<th class='is-uppercase'>");
  echo $fieldMetadata["Name"];
  echo ("</th>");
}
echo ("<th><th><th></tr>");

while ($row = sqlsrv_fetch_array($getQuestions, SQLSRV_FETCH_ASSOC)) {
  echo ("<tr>");
  foreach($row as $col){
    echo ("<td>");
    if(is_a($col, 'DateTime')) {
      $col = $col->format('Y-m-d');
    }
    echo (is_null($col) ? "" : $col);
    echo ("</td>");
  }

  echo ("<td><form method='post' action='./remove-question.php'><input type='hidden' name='questionaireID' value='".$_GET['questionaireID']."'><button class='button is-danger' type='submit' name='questionID' value='".$row['question_id']."'>Remove</button></form></td>");
  echo ("<td><form method='post'><input type='hidden' name='oldID' value='".$row['question_id']."'><input type='hidden' name='questionaireID' value='".$_GET['questionaireID']."'>".$questionSelect."</td><td><button class='button is-".$color."' type='submit'>Exchange</button></form></td>");
  echo ("</tr>");
}
echo ("</table>");
?>
  </div>
</body>
</html>
