<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(0);

$getQuery = sqlsrv_query(
  $conn,
  "EXEC sp_question_count_in_questionare",
  );
?>

<html>
<head>
  <title>Questionaires and question number</title>
  <link rel="stylesheet" href="../bulma.css">
</head>
<body>
  <div class="container">
    <h1 class="title">Questionaires and question number</h1>

    <a class="button is-text" href="./">Home</a>

    <?php PrintResultSet($getQuery) ?>
  </div>
</body>
</html>
