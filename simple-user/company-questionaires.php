<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(0);

$getCompanyQuestionaires = sqlsrv_query(
  $conn,
  "EXEC sp_questions_of_user ?",
    [$_SESSION['company_id'] ?? $_GET['companyID']]
  );
?>

<html>
<head>
  <title>Company Questionaires</title>
  <link rel="stylesheet" href="../bulma.css">
</head>
<body>
  <div class="container">
    <h1 class="title">Company Questionaires</h1>

    <a class="button is-text" href="./">Home</a>
    <?php if(!$_SESSION['company_id']) { ?>
      <form method="get">
        <div class="field">
          <label class="label">Company ID</label>
          <input class="input" value="<?php echo $_GET['companyID'] ?>" type="number" name="companyID">
        </div>
        <input class="button is-primary" type="submit">
      </form>
    <?php } ?>

    <?php PrintResultSet($getCompanyQuestionaires) ?>
  </div>
</body>
</html>
