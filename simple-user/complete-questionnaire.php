<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(0);

$errors = '';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  if (isset($_POST['questionaireID'])) {
    $query = sqlsrv_query(
      $conn,
      "{CALL P_finalize_questionaire(?, ?, ?)}",
      [$_SESSION['id'], $_SESSION['company_id'], $_POST['questionaireID']]
    );

    if( $query === false   ) {
      $errors = $errors.sqlsrv_errors()[0]['message'];
    }
    header("Location: ./manage-questionnaires.php?errors=".nl2br($errors));
  } else {
    header("Location: ./update-questionnaire.php?questionaireID=".$_POST['questionaireID']."&errors=".nl2br($errors));
  }
}
?>
