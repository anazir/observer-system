<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(0);
forbid(2);

$questionID = $_GET['questionID'];
echo $_GET['errors'];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $userID = $_SESSION['id'];
  $companyID = $_SESSION['company_id'];
  $description = $_POST['description'] ?: null;
  $text = $_POST['text'] ?: null;
  $type = $_POST['type'];
  $type = $type == '' ? null : (int)$type;
  echo var_dump($type);

  if (empty($questionID) && $questionID != 0) echo "Question id is empty!<br/>";

  if (!empty($questionID) || $questionID == 0) {
    $query = sqlsrv_query(
      $conn,
      "{CALL P_update_question(?, ?, ?, ?, ?)}",
      [$userID, $questionID, $description, $text, $type]
    );

    if( $query === false   ) {
      echo print_r( sqlsrv_errors(), true  );
    }
  }
}

$getQuestions = sqlsrv_query(
  $conn,
  "{CALL P_get_question_info(?, ?)}",
  [$_SESSION['company_id'], $questionID]
);

$getChoices = sqlsrv_query(
  $conn,
  "{CALL P_get_choices(?)}",
  [$questionID]
);

$color = ['warning', 'info', 'primary'][$_SESSION['user_type']];
?>

<html>
<head>
  <title>Update Question</title>
  <link rel="stylesheet" href="../bulma.css">
</head>
<body>
  <div class="container">
    <h1 class="title">Update Question with ID <?php echo $_GET['questionID'] ?></h1>
    <div class="block">
      <a class="button is-text" href="./">Home</a>
    </div>

    <div class="box">
      <h3 class="title is-4">Question</h3>

      <form method="post">
        <input name="questionID" value="<?php echo $_GET['questionID'] ?>" type="hidden">
        <div class="field">
          <label class="label">New Description</label>
          <textarea class="textarea" name="description"><?php echo ($row = sqlsrv_fetch_array($getQuestions))['description'] ?></textarea><br>
        </div>
        <div class="field">
          <label class="label">New Text</label>
          <textarea class="textarea" name="text"><?php echo $row['text'] ?></textarea><br>
        </div>
        <div class="field">
          <label class="label">New Type</label>
            <div class="select is-fullwidth">
              <select name="type">
              <option value=''>Leave as is</option>
              <option value='0'>Text</option>
              <option value='1'>Multiple Choice with single answer</option>
              <option value='2'>Multiple Choice with multiple answers</option>
              <option value='3'>Number without limits</option>
              <option value='4'>Number with limits</option>
            </select>
            </div>
        </div>
        <input class="button is-<?php echo $color ?>" type="submit" name="connect">
      </form>
    </div>

    <?php if($row['type'] == 'Numeric question with bounds') { ?>
      <div class="box">
        <h3 class="title is-4">Modify Bounds</h3>
        <form method="post" action="./bounds.php">
          <div class="field">
            <label class="label">New Min</label>
            <input class="input" type="number" name="min" value="<?php echo $row['min'] ?>"><br>
          </div>
          <div class="field">
            <label class="label">New Max</label>
            <input class="input" type="number" name="max" value="<?php echo $row['max'] ?>"><br>
          </div>
          <input type="hidden" name="edit" value="<?php echo isset($row['min']) ? 'true' : 'false' ?>">
          <input type="hidden" name="questionID" value="<?php echo $questionID ?>">
          <input class="button is-<?php echo $color ?>" type="submit" name="connect"> 
        </form>
      </div>
    <?php } ?>

    <?php if($row['type'][0] == 'M') { ?>
      <div class="box">
        <h3 class="title is-4">Add New Choice</h3>
        <form method="post" action="./create-choice.php">
          <div class="field">
            <label class="label">Text</label>
            <input class="input" type="text" name="text"><br>
          </div>
          <input type="hidden" name="questionID" value="<?php echo $questionID ?>">
          <input class="button is-<?php echo $color ?>" type="submit" name="connect"> 
        </form>
      </div>
    <?php } ?>

<?php 
if($row['type'][0] == 'M') {
  echo ("<p class='title'>Results</p><table class='table'><tr><th class='is-uppercase'>Text</th><th><th></tr>");

  while ($row = sqlsrv_fetch_array($getChoices, SQLSRV_FETCH_ASSOC)) {
    echo ("<tr>");
    if($row['id'] == $_GET['choiceID']) {
      echo ("<form method='post' action='./update-choice.php'><input type='hidden' name='choiceID' value='".$row['id']."'><input type='hidden' name='questionID' value='".$questionID."'><td><input class='input' type='text' name='text' value='".$row['text']."'></td><td><button class='button is-".$color."' type='submit''>Submit</button></td></form>");
    } else {
      echo ("<td>".$row['text']."</td>");
      echo ("<td><form method='get'><input type='hidden' name='questionID' value='".$questionID."''><button class='button is-".$color."' type='submit' name='choiceID' value='".$row['id']."'>Edit</button></form></td>");
    }
    echo ("<td><form method='post' action='./delete-choice.php'><input type='hidden' value='".$questionID."' name='questionID'><button class='button is-danger' type='submit' name='choiceID' value='".$row['id']."'>Delete</button></form></td>");
    echo ("</tr>");
  }
  echo ("</table>");
}
?>
  </div>
</body>
</html>
