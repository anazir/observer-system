<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(0);

$errors = '';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $userID = $_SESSION['id'];
  $companyID = $_SESSION['company_id'];
  $questionID = $_POST['questionID'];
  $text = $_POST['text'];

  if (empty($userID)) $errors = $errors. "User ID is empty!<br/>";
  if (empty($companyID)) $errors = $errors. "Description is empty!<br/>";
  if (empty($tetx)) $errors = $errors. "Text is empty!<br/>";

  if (!empty($userID) && !empty($companyID) && !empty($text)) {
    $query = sqlsrv_query(
      $conn,
      "{CALL P_add_choice(?, ?, ?, ?)}",
      [$userID, $companyID, $questionID, $text]
    );

    if( $query === false   ) {
      $errors = $errors.sqlsrv_errors()[0]['message'];
    }
  }
}

header("Location: ./update-question.php?questionID=".$questionID."&errors=".nl2br($errors));
?>
