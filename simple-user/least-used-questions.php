<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(0);

$getQuery = sqlsrv_query(
  $conn,
  "EXEC P_get_least_used_questions ?",
    [$_GET['num'] ?? 0]
  );

$color = ['warning', 'info', 'primary'][$_SESSION['user_type']];
?>

<html>
<head>
  <title>Least used questions</title>
  <link rel="stylesheet" href="../bulma.css">
</head>
<body>
  <div class="container">
    <h1 class="title">Least used questions</h1>

    <a class="button is-text" href="./">Home</a>
    <form>
      <div class="field">
        <label class="label">Number of questions</label>
          <input class="input" name="num">
      </div>
      <input class="button is-<?php echo $color ?>" type="submit">
    </form>

    <?php if(isset($_GET['num'])) { ?>
      Number given: <?php echo $_GET['num'] ?>
    <?php } ?>
    <?php PrintResultSet($getQuery) ?>
  </div>
</body>
</html>
