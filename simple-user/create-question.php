<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(0);

$errors = '';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $userID = $_SESSION['id'];
  $description = $_POST['description'];
  $text = $_POST['text'];
  $type = $_POST['type'];

  if (empty($userID)) $errors = $errors. "User ID is empty!<br/>";
  if (empty($description)) $errors = $errors. "Description is empty!<br/>";
  if (empty($text)) $errors = $errors. "Text is empty!<br/>";
  if (myEmpty($type)) $errors = $errors. "Type is empty!<br/>";

  if (!empty($userID) && !empty($description) && !empty($text) && !myEmpty($type)) {
    $query = sqlsrv_query(
      $conn,
      "{CALL P_create_question(?, ?, ?, ?)}",
      [$description, $text, $type, $userID]
    );

    if( $query === false   ) {
      $errors = $errors. print_r( sqlsrv_errors(), true  );
    }
  }
}

header("Location: ./manage-questions.php?errors=".$errors);
?>
