<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(1);

$getCompaniesAndAdmins = sqlsrv_query(
  $conn,
  "{CALL P_get_simple_user_info(?, ?)}",
  [$_SESSION['company_id'] ?? $_GET['companyID'], $_GET['search']]
);

$color = $_SESSION['user_type'] == 2 ? 'primary' : 'info';
?>

  <html>
<head>
  <title>Select User to update</title>
  <link rel="stylesheet" href="../bulma.css">
</head>
<body>
  <div class="container">
    <h1 class="title">Select User to update</h1>

    <a class="button is-text" href="./">Home</a>

    <section class="box">
      <form method="get">
        <?php if(!$_SESSION['company_id']) { echo '<div class="field"><label class="label">Company</label><input class="input" value="'.$_GET['companyID'].'" type="number" name="companyID"><br></div>'; } ?>
        <div class="field">
          <label class="label">Search by Name, Username or Job Title</label>
            <input class="input" type="text" name="search"><br>
        </div>
        <input class="button is-<?php echo $color ?>" type="submit" name="connect">
      </form>
    </section>

<?php 
echo ("<table class='table is-fullwidth'><tr >");

foreach( sqlsrv_field_metadata($getCompaniesAndAdmins) as $fieldMetadata ) {
  echo ("<th class='is-uppercase'>");
  echo $fieldMetadata["Name"];
  echo ("</th>");
}
echo ("<th></tr>");

while ($row = sqlsrv_fetch_array($getCompaniesAndAdmins, SQLSRV_FETCH_ASSOC)) {
  echo ("<tr>");
  foreach($row as $col){
    echo ("<td>");
    if(is_a($col, 'DateTime')) {
      $col = $col->format('Y-m-d');
    }
    echo (is_null($col) ? "Null" : $col);
    echo ("</td>");
  }

  if($_SESSION['company_id']) {
    echo ("<td><form method='get' action='./update-users.php'><button class='button is-info' type='submit' name='oldID' value='".$row['id_number']."'>Select</button></form></td>");
  }
  echo ("</tr>");
}
echo ("</table>");
?>
  </div>
</body>
</html>
