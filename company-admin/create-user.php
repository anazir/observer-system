<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(1);
forbid(2);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $idNum = $_POST['idNum'];
  $name = $_POST['name'];
  $username = $_POST['username'];
  $password = $_POST['password'];
  $sex = $_POST['sex'];
  $jobTitle = $_POST['jobTitle'];
  $birthdate = $_POST['birthdate'];
  $company = $_SESSION['company_id'];

  if (empty($idNum)) echo "Id Number is empty!<br/>";
  if (empty($name)) echo "Name is empty!<br/>";
  if (empty($username)) echo "Username is empty!<br/>";
  if (empty($password)) echo "Password is empty!<br/>";
  if (empty($sex) && $sex != 0) echo "Sex is empty!<br/>";
  if (empty($jobTitle)) echo "Job Title is empty!<br/>";
  if (empty($birthdate)) echo "Birthdate is empty!<br/>";

  if (!(
    empty($idNum) ||
    empty($name) ||
    empty($username) ||
    empty($password) ||
    empty($sex) && $sex != 0 ||
    empty($jobTitle) ||
    empty($birthdate)
  )) {
    $query = sqlsrv_query(
      $conn,
      "{CALL P_new_simple_user(?, ?, ?, ?, ?, ?, ?, ?)}",
      [$company, $idNum, $name, $username, $password, $sex, $jobTitle, $birthdate]
    );

    if( $query === false  ) {
      echo print_r( sqlsrv_errors(), true );
    }
  }
}

$getUser = sqlsrv_query(
  $conn,
  "{CALL P_get_simple_user_info(?)}",
  [$_SESSION['company_id']]
);
?>

<html>
<head>
  <title>Create new Simple User</title>
  <link rel="stylesheet" href="../bulma.css">
</head>
<body>
  <div class="container">
    <h1 class="title">Create new Simple User</h1>

    <div class="block">
      <a class="button is-text" href="./">Home</a>
    </div>

    <div class="box">
      <form method="post">
        <div class="field">
          <label class="label">ID Number</label>
          <input class="input" type="number" name="idNum"><br>
        </div>
        <div class="field">
          <label class="label">Name</label>
          <input class="input" type="text" name="name"><br>
        </div>
        <div class="field">
          <label class="label">Username</label>
          <input class="input" type="text" name="username"><br>
        </div>
        <div class="field">
          <label class="label">Password</label>
          <input class="input" type="password" name="password"><br>
        </div>
        <div class="field">
          <label class="label">Sex</label>
          <div class="select is-fullwidth">
            <select name="sex">
              <option value='0'>Male</option>
              <option value='1'>Female</option>
            </select>
          </div>
        </div>
        <br>
        <div class="field">
          <label class="label">Job Title</label>
          <input class="input" type="text" name="jobTitle"><br>
        </div>
        <div class="field">
          <label class="label">Birth Date</label>
          <input class="input" type="date" name="birthdate"><br>
        </div>
        <input class="button is-info" type="submit" name="connect"> 
      </form>
    </div>

    <?php PrintResultSet($getUser) ?>
  </div>
</body>
</html>
