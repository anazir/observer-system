<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/db.php');

include_once '../helpers/functions.php';
authenticate(1);
forbid(2);

$oldID = $_GET['oldID'];
$companyID = $_SESSION['company_id'];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $name = $_POST['name'] ?: null;
  $username = $_POST['username'] ?: null;
  $password = $_POST['password'] ?: null;
  $sex = [
    '' => null,
    '1' => 1,
    '0' => 0
  ][$_POST['sex']];
  $jobTitle = $_POST['jobTitle'] ?: null;
  $birthdate = $_POST['birthdate'] ?: null;

  if (empty($oldID) && $oldID != 0) echo "Old id is empty!<br/>";

  echo $sex;
  if (!empty($oldID) || $oldID == 0) {
    $query = sqlsrv_query(
      $conn,
      "{CALL P_modify_simple_user(?, ?, ?, ?, ?, ?, ?, ?)}",
      [$companyID, $oldID, $name, $username, $password, $sex, $jobTitle, $birthdate]
    );

    if( $query === false   ) {
      echo print_r( sqlsrv_errors(), true  );
    }
  }
}

$getUsers = sqlsrv_query(
  $conn,
  "{CALL P_get_simple_user_info(?)}",
  [$companyID]
);
?>

<html>
<head>
  <title>Update User</title>
  <link rel="stylesheet" href="../bulma.css">
</head>
<body>
  <div class="container">
    <h1 class="title">Update User with ID <?php echo $_GET['oldID'] ?></h1>
    <div class="block">
      <a class="button is-text" href="./">Home</a>
    </div>

    <div class="box">
      <h3 class="title is-4">User</h3>
      <form method="post">
        <input name="oldID" value="<?php echo $_GET['oldID'] ?>" type="hidden">
        <div class="field">
          <label class="label">New Name</label>
          <input class="input" type="text" name="name"><br>
        </div>
        <div class="field">
          <label class="label">New Username</label>
          <input class="input" type="text" name="username"><br>
        </div>
        <div class="field">
          <label class="label">New Password</label>
          <input class="input" type="password" name="password"><br>
        </div>
        <div class="field">
          <label class="label">New Sex</label>
          <div class="select is-fullwidth">
            <select name="sex">
              <option value=''>Leave as is</option>
              <option value='0'>Male</option>
              <option value='1'>Female</option>
            </select>
          </div>
        </div>
        <div class="field">
          <label class="label">New Job Title</label>
          <input class="input" type="text" name="jobTitle"><br>
        </div>
        <div class="field">
          <label class="label">New Birth Date</label>
          <input class="input" type="date" name="birthdate"><br>
        </div>
        <input class="button is-info" type="submit" name="connect">
      </form>
    </div>

    <?php PrintResultSet($getUsers) ?>
  </div>
</body>
</html>
