<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  session_start();

  if (isset($_POST['connect'])) {
    echo "<br/>Setting session variables!<br/>";
    // collect value of input field
    $username = $_POST['userName'];
    $password = $_POST['pswd'];
    $userType = $_POST['user-type'];

    if (empty($username)) echo "Username is empty!<br/>";
    if (empty($password)) echo "Password name is empty!<br/>";
    if (empty($userType) && $userType != 0) echo "User Type is empty!<br/>";

    if (empty($username) || empty($password) || empty($userType) && $userType != 0) {
      session_unset();
      session_destroy();
    } else {
      include_once('./helpers/db.php');

      $getUsers = sqlsrv_query(
        $conn,
        "SELECT id_number, username, user_type, company_id FROM [user] WHERE username = ? AND password = ? AND user_type = ?",
        [$username, $password, $userType]
      );

      $user = sqlsrv_fetch_array($getUsers);

      if($user) {
        $_SESSION['id'] = $user['id_number'];
        $_SESSION['username'] = $user['username'];
        $_SESSION['user_type'] = $user['user_type'];
        $_SESSION['company_id'] = $user['company_id'];

        switch($userType) {
        case 2:
          $userEntity = 'observer-admin';
          break;
        case 1:
          $userEntity = 'company-admin';
          break;
        case 0:
          $userEntity = 'simple-user';
          break;
        }
        include_once './helpers/functions.php';
        header("Location: ".$userEntity."/");
      }
    }
  }
}
?>

<html>
<head>
  <title>Login</title>
  <link rel="stylesheet" href="./bulma.css">
</head>
<body>
  <div class="columns is-vcentered">
    <div class="login column is-4 ">
      <section class="section">
        <div class="has-text-centered">
          <p class="title">Login</p>
        </div>

        <form method="post">
          <div class="field">
            <label class="label">Username</label>
            <div class="control">
              <input class="input" name="userName" type="text">
            </div>
          </div>

          <div class="field">
            <label class="label">Password</label>
            <div class="control">
              <input class="input" type="password" name="pswd">
            </div>
          </div>

          <div class="field">
            <label class="label">User Type</label>
            <div class="control">
              <div class="select is-fullwidth">
                <select name="user-type">
                  <option value="0">Simple user</option>
                  <option value="1">Company Admin</option>
                  <option value="2">Observer Admin</option>
                </select>
            </div>
            </div>
          </div>

          <div class="has-text-centered">
            <button class="button is-vcentered is-primary is-outlined" name="connect">Login</button>
          </div>
        </form>
      </section>
    </div>
    <div class="hero is-primary is-fullheight column is-8">
      <div class="hero-body">
        <p class="title is-size-1 is-fullwidth">Observer System |  </p>
        <p class="subtitle">Questionnaire management system</p>
      </div>
    </div>
  </div>
</body>
</html>
