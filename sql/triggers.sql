go
if OBJECT_ID('TR_question_del_upd','TR') is not null
	drop trigger [TR_question_del_upd]
--	Stops delete/update when a question is in use
go
create trigger [TR_question_del_upd] on [question]
after delete, update
as
	if exists (
		select *
		from deleted as d, [questionaire_questions] as [qq]
		where d.[question_id] = qq.[question_id]
	)

	begin
		print(N'Cannot change or delete a question that is in use.');
		rollback transaction;
	end


go
drop trigger if exists [TR_bounds_modify_check]
--	Stops modifying questions that are in use
go 
create trigger [TR_bounds_modify_check] on [bounds]
after insert, delete, update
as
	declare @questionID int;
	if exists (select * from inserted)
		set @questionID = (select [question] from inserted)
	else
		set @questionID = (select [question] from deleted)

	if exists( select * from [questionaire_questions] where [question_id] = @questionID)
	begin
		print(N'Cannot modify a question that is in use.');
		rollback transaction;
	end

go
drop trigger if exists [TR_multich_modify_check]
--	Stops modifying questions that are in use
go 
create trigger [TR_multich_modify_check] on [multiple_choice]
after insert, delete, update
as
	declare @questionID int;
	if exists (select * from inserted)
		set @questionID = (select [question] from inserted)
	else
		set @questionID = (select [question] from deleted)

	if exists( select * from [questionaire_questions] where [question_id] = @questionID)
	begin
		print(N'Cannot modify a question that is in use.');
		rollback transaction;
	end

	go
if OBJECT_ID('TR_questionaire_upd','TR') is not null
	drop trigger [TR_questionaire_upd]
-- Stops updates on finished questionaires
go
create trigger [TR_questionaire_upd] on [questionaire]
after update
as

if exists (
	select *
	from deleted as d
	where d.[isComplete] = 1
)

begin
	print('Cannot change a completed questionaire.');
	rollback transaction
return
end

