create table [dbo].[user](
	[id_number] int primary key,
	[name]	varchar(63) not null,
	[username] varchar(63) not null unique,
	[password] varchar(63) not null,
	[sex] bit not null,			-- 0=M, 1=F
	[job_title] varchar(100),
	[birthdate] date not null,
	[user_type] int not null,	-- 0= Simple User, 1= Company Manager, 2= Observer manager
	[company_id] int -- foreign key
	
)

alter table [user] add constraint [ckh_valid_sex] check (sex in (0,1));
alter table [user] add constraint [chk_valid_user_type] check ( [user_type] in (0,1,2) );


create table [dbo].[company](
	[registration_id] int primary key,
	[name] varchar(63) not null,
	[registration_date] date not null
)

create table [dbo].[question](
	[question_id] int identity(1,1) primary key,
	[description] varchar(63) not null,
	[text] varchar(255) not null,	
	[type] int not null,	-- 0= Text, 1= Multiple Choice Single Ans, 2=Mult Choice Mult Ans, 3= Number, no limits, 4= Number with limits
	[user_id] int -- foreign key
)

alter table [question] add constraint [chk_valid_type] check (type in (0,1,2,3,4) );

create table [dbo].[multiple_choice](
	[id] int identity(1,1) primary key,
	[text] varchar(255) not null,
	[question] int not null -- foreign key
)

create table [dbo].[bounds](
	[id] int identity(1,1) primary key,
	[min] int,
	[max] int,
	[question] int not null, --foreign key
)

create table [dbo].[questionaire](
	[questionaire_id] int identity(1,1) primary key,
	[title] varchar(63),
	[version] int not null,
	[isComplete] bit not null,
	[creator_id] int, -- foreign key
	[previus_version] int, -- foreign key

	constraint uq unique ([title], [version])
)

create table [dbo].[questionaire_questions](
	[questionaire_id] int not null, -- foreign key
	[question_id] int not null, -- foreign key

	constraint pk_questionaire_questions primary key ([questionaire_id], [question_id])
)


create table [dbo].[log] (
	[log_id] int identity(1,1) primary key,
	[user_id] int not null, -- foreign key 
	[change_date] date not null, 
	[change_on] bit not null, -- 0=question, 1=questionaire
	[changed] int	-- id of changed value
)

-- FK constrainst


alter table [questionaire_questions]  with check
add constraint [fk_questionaire]
foreign key ([questionaire_id])
references [questionaire] ([questionaire_id])
on delete cascade

alter table [questionaire_questions]  with check
add constraint [fk_question]
foreign key ([question_id])
references [question] ([question_id])
on delete cascade

alter table [questionaire] with check
add constraint [fk_parent]
foreign key ([previus_version])
references [questionaire] ([questionaire_id])

alter table [user] with check
add constraint [fk_company]
foreign key ([company_id])
references [company] ([registration_id])
on update cascade

alter table [multiple_choice] with check
add constraint [fk_main_question_multiple_choice]
foreign key ([question])
references [question] ([question_id])
on delete cascade

alter table [bounds] with check
add constraint [fk_main_question_bounds]
foreign key ([question])
references [question] ([question_id])
on delete cascade

alter table [questionaire] with check
add constraint [fk_question_creator]
foreign key ([creator_id])
references [user] ([id_number])
on update cascade
on delete set null

alter table [question] with check
add constraint [fk_questionaire_creator]
foreign key ([user_id])
references [user] ([id_number])
on update cascade
on delete set null

alter table[log] with CHECK
add constraint [fk_change_user]
foreign key ([user_id])
references [user] ([id_number])
on update cascade