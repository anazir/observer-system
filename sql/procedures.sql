 --	Functions used by these procedures

 go
drop function if exists fu_company_questionare
--	Returns all questionaires of a company
go
create function fu_company_questionare (@company_id int)
returns table
as
return (
	select qe.questionaire_id
	from [dbo].questionaire qe
	where qe.isComplete=1 and qe.creator_id in(
		select u.id_number
		from  [dbo].[user] u
		where u.[company_id] = isnull(@company_id,u.[company_id])
	)
)

go
drop function if exists [F_get_type_string]
--	Translates type number into string
go
create function [dbo].[F_get_type_string](@typeint int)
returns varchar(63)
as
begin
	declare @typestring varchar(63);

	if @typeint = 0
		set @typestring = 'Freetext Question';
	if @typeint = 1
		set @typestring = 'Multiple Choice Question with Single Answer';
	if @typeint = 2
		set @typestring = 'Multiple Choice Question with Multiple Answers';
	if @typeint = 3
		set @typestring = 'Numeric question with no bounds';
	if @typeint = 4
		set @typestring = 'Numeric question with bounds';
	
	return @typestring
end


go


drop function if exists fu_question_popularity
go
-- Returns questionID and the number of questionaires each question of a company is in
create function fu_question_popularity(@company_id int)
returns table
as
return(
	select count(qq.questionaire_id) as questionaire_count, qq.question_id
	from [questionaire_questions] qq, fu_company_questionare(@company_id) cq
	where qq.questionaire_id = cq.questionaire_id
	group by qq.question_id 
)

go
drop function if exists fu_question_count_in_questionare
--	Returns the number of questions in each questionaire of a company
go
create function fu_question_count_in_questionare (@company_id int)
returns table
as
return(
	select count(qq.[question_id]) as question_count, qq.questionaire_id
	from [questionaire_questions] qq, fu_company_questionare(@company_id) cq
	where qq.questionaire_id = cq.questionaire_id
	group by qq.questionaire_id 
)

	
go
drop function if exists fu_question_average_in_questionare
--	Returns the average number of questions per questionaire of a company
go
create function fu_question_average_in_questionare (@company_id int)
returns table
as
return (
	select avg(cast(qcq.question_count as float)) as question_average
	from fu_question_count_in_questionare(@company_id) qcq
)


-- Logs
go
drop procedure if exists [sp_create_log]
--	Creates a new log
go
create procedure [sp_create_log]
@user_id int,
@change_on bit,
@changed int
as
begin transaction
print @@ROWCOUNT
	insert into [log] ([user_id],[change_date],[change_on],[changed])
	values(@user_id, GETDATE(), @change_on, @changed )
	if( @@rowcount = 0)
		rollback transaction
	else
		commit transaction
go 
	

--			Q1
go
if OBJECT_ID('P_create_company','P') is not null
	drop procedure [P_create_company]
--	Creates new company and it's manager
go
create procedure [P_create_company]
@compID int,
@compName varchar(20),
@manID int,
@manName varchar(20),
@manUsername varchar(20),
@manPassword varchar(20),
@manSex varchar(20),
@manJobTitle varchar(100),
@manBirthdate date
as
begin transaction
print @@ROWCOUNT
	insert into [company] ([registration_id],[name],[registration_date])
	values(@compID, @compName, GETDATE())

	if( @@rowcount = 0)
		rollback transaction

	insert into [user] ([id_number],[name], [username], [password], [sex], [job_title], [birthdate], [user_type], [company_ID])
	values(@manID, @manName, @manUsername, @manPassword, @manSex, @manJobTitle, @manBirthdate, 1, @compID)

	if( @@ROWCOUNT = 0)
		rollback transaction
	else
		commit transaction

--			Q2
go 
if OBJECT_ID('P_get_company_info','P') is not null
	drop procedure [P_get_company_info]
--	Returns company information
go
create procedure [P_get_company_info]
@searchfor varchar(100) = null	-- Optional for searching
as

	select c.[registration_id], c.name as [Company_Name], c.registration_date, u.name as [Admin_Name], case when u.sex = 0 then 'Male' else 'Female' end as sex , u.birthdate, u.job_title
	from company c JOIN [user] u on c.registration_id = u.company_id
	where u.user_type = 1 AND(
	c.[name] like CONCAT('%', @searchfor, '%') OR
	u.[name] like CONCAT('%', @searchfor, '%') OR
	u.[username] like CONCAT('%', @searchfor, '%')
	)


go
drop procedure if exists [P_get_company_manager]
--	Returns the manager of a company
go
create procedure [P_get_company_manager]
@companyID int
as
	select [id_number]
	from [user]
	where [company_id] = @companyID AND [user_type] = 1


go
if OBJECT_ID('P_modify_company','P') is not null
	drop procedure [P_modify_company]
--	Updates a company
go
create procedure [P_modify_company]
@compID int,
@newID int = null,
@newName varchar(20) = null,
@newManager int = null
as
	if(@newName is not null)
		update company
		set [name] = @newName
		where registration_id = @compID

	if (@newManager is not null)
	begin
		begin transaction
		-- Remove old manager
		update [user]
		set user_type = 0
		where [user_type] = 1 AND [company_id] = @compID
		if( @@ROWCOUNT = 0)
			rollback transaction

		-- Promote new manager 
		update [user]
		set user_type = 1
		where [user_type] = 0 AND [company_id] = @compID AND [id_number] = @newManager
		if( @@ROWCOUNT = 0)
			rollback transaction
		else
			commit transaction
	end
		
		if(@newID is not null)
			update company
			set [registration_id] = @newID
			where [registration_id] = @compID
			
go
if OBJECT_ID('P_modify_manager','P') is not null
	drop procedure [P_modify_manager]
--	Updates the manager of a company
go
create procedure [P_modify_manager]
@userID int,
@newName varchar(20) = null,
@newSex bit = null,
@newTitle varchar(100) = null,
@newBirthdate date = null,
@newUsername varchar(20) = null,
@newPass varchar(20) = null
as
	update [user]
	set
	[name] = COALESCE(@newName, [name]),
	[sex] = COALESCE(@newSex, [sex]),
	[job_title] = COALESCE(@newTitle, [job_title]),
	[birthdate] = COALESCE(@newBirthdate, [birthdate]),
	[username] = COALESCE(@newUsername, [username]),
	[password] = COALESCE(@newPass, [password])
	where [id_number] = @userID AND [user_type] = 1


--			Q3
go
if OBJECT_ID('P_new_simple_user','P') is not null
	drop procedure [P_new_simple_user]
-- Inserts simple user
go
create procedure [P_new_simple_user]
@compID int,
@idNum int,
@name varchar(20),
@username varchar(20),
@password varchar(20),
@sex bit,
@title varchar(100),
@birthdate date
as
	insert into [user](id_number, [name], username, [password], sex, [job_title], birthdate, [user_type], [company_id])
	values (@idNum, @name, @username, @password, @sex, @title, @birthdate, 0, @compID)


--			Q4
go
if OBJECT_ID('P_get_simple_user_info','P') is not null
	drop procedure [P_get_simple_user_info]
--	Returns the info of simple users
go
create procedure [P_get_simple_user_info]
@compID int,
@searchfor varchar(100) = null
as
	select u.[id_number], u.[name], u.[username], case when u.sex = 0 then 'Male' else 'Female' end as sex, u.[job_title], u.[birthdate]
	from [user] u
	where u.[user_type] = 0 AND u.[company_id] = @compID AND(
	u.[name] like CONCAT('%', @searchfor, '%') OR
	u.[username] like CONCAT('%', @searchfor, '%') OR
	u.[job_title]like CONCAT('%', @searchfor, '%')	
	)
go
if OBJECT_ID('P_modify_simple_user','P') is not null
	drop procedure [P_modify_simple_user]
--	Updates a simple user
go
create procedure [P_modify_simple_user]
@companyID int,
@userID int,
@name varchar(20)= null,
@username varchar(20)= null,
@pass varchar(20)= null,
@sex bit= null,
@title varchar(100)= null,
@bday date= null
as

	update [user]
	set
	[name] = COALESCE(@name, [name]),
	[sex] = COALESCE(@sex, [sex]),
	[job_title] = COALESCE(@title, [job_title]),
	[birthdate] = COALESCE(@bday, [birthdate]),
	[username] = COALESCE(@username, [username]),
	[password] = COALESCE(@pass, [password])
	where [id_number] = @userID AND [user_type] = 0 AND [company_id] = @companyID


--			Q5
go
drop procedure if exists [P_get_question_info]
--	Returns information of the questions of a company
go
create procedure [P_get_question_info]
@companyID int,
@questionID int = null -- Optional to only get a specific question
as
	select q.[question_id], q.[description], q.[text], [dbo].[F_get_type_string](q.[type]) as [type] , q.[user_id], b.[min], b.[max]
	from [question] q
	LEFT JOIN [bounds] b on q.[question_id] = b.[question]
	where q.[user_id] in(	select u.[id_number]
							from [user] u
							where u.[company_id] = @companyID) AND
	(@questionID is null OR @questionID = q.[question_id] )


go
drop procedure if exists [P_get_choices]
--	Returns the choices of a multiple choice question
go
create procedure [P_get_choices]
@questionID int
as
	select m.id, m.text
	from [multiple_choice] m
	where m.[question] = @questionID

go
drop procedure if exists [P_create_question]
--	Creates a new question
go
create procedure [P_create_question]
@description varchar(20),
@text varchar(255),
@type int,
@userID int
as
	insert into [question]([description], [text], [type], [user_id])
	values(@description, @text, @type, @userID)


go 
drop procedure if exists [P_delete_question]
--	Deletes a question
go 
create procedure [P_delete_question]
@userID int,
@companyID int,
@questionID int
as
	delete from [question] where [question_id] = @questionID AND [user_id] in	(select u.[id_number]
																				from [user] u
																				where u.[company_id] = @companyID)
	exec sp_create_log @userID, 0, @questionID;

go
drop procedure if exists [P_add_bounds]
-- Insert bounds to a question
go
create procedure [P_add_bounds]
@userID int,
@companyID int,
@questionID int,
@min int,
@max int
as
	if( (select [user_id] from [question] where [question_id] = @questionID) in (select [id_number] from [user] where [company_id] = @companyID ) )	-- Security check
		if((select q.type from [question] q where q.[question_id] = @questionID) = 4)
		begin
			insert into [bounds]([question], [min], [max])
			values(@questionID, @min, @max)
			exec sp_create_log @userID, 0, @questionID
		end else
			print('Cannot add bounds to a not-type-4 question')


go
drop procedure if exists [P_modify_bounds]
-- Updates the bounds of a question
go
create procedure [P_modify_bounds]
@userID int,
@companyID int,
@questionID int,
@min int = null,
@max int = null
as
	if( (select [user_id] from [question] where [question_id] = @questionID) in (select [id_number] from [user] where [company_id] = @companyID ) )	-- Security check
	begin	
		update [bounds] set 
		[max] = COALESCE(@max, [max]),
		[min] = COALESCE(@min, [min])
		where [question] = @questionID
		exec sp_create_log @userID, 0, @questionID
	end

go
drop procedure if exists [P_remove_bounds]
--	 Remove bounds of a question
go
create procedure [P_remove_bounds]
@userID int,
@companyID int,
@questionID int
as
	if( (select [user_id] from [question] where [question_id] = @questionID) in (select [id_number] from [user] where [company_id] = @companyID ) )	-- Security check
	begin
		delete from bounds where [question] = @questionID
		exec sp_create_log @userID, 0, @questionID
	end

go
drop procedure if exists [P_add_choice]
--	Add choice to multiple choice question
go
create procedure [P_add_choice]
@userID int,
@companyID int,
@questionID int,
@text varchar(255)
as
	if( (select [user_id] from [question] where [question_id] = @questionID) in (select [id_number] from [user] where [company_id] = @companyID ) )	-- Security check
		if((select q.type from [question] q where q.[question_id] = @questionID) in (1,2) )
		begin
			insert into [multiple_choice]([question], [text])
			values(@questionID, @text)
			exec sp_create_log @userID, 0, @questionID
		end

		else
			print('Cannot add choice to a not-multiple-choice question')


go
drop procedure if exists [P_modify_choice]
--	Updates a choice of a multiple choice question
go
create procedure [P_modify_choice]
@userID int,
@companyID int,
@choiceID int,
@questionID int,
@text varchar(255)
as
	if( (select [user_id] from [question] where [question_id] in (select [question] from [multiple_choice] where [id] = @choiceID) ) in (select [id_number] from [user] where [company_id] = @companyID ) )	-- Security check
	begin
		update [multiple_choice] set
		[text] = COALESCE(@text, [text])
		where [id] = @choiceID
		exec sp_create_log @userID, 0, @questionID
	end


go 
drop procedure if exists [P_remove_choice]
--	Remove choice from multiple choice question
go 
create procedure [P_remove_choice]
@userID int,
@questionID int,
@companyID int,
@choiceID int
as
	if( (select [user_id] from [question] where [question_id] in (select [question] from [multiple_choice] where [id] = @choiceID) ) in (select [id_number] from [user] where [company_id] = @companyID ) )	-- Security check
	begin		
		delete from [multiple_choice] where [id] = @choiceID
		exec sp_create_log @userID, 0, @questionID
	end


go 
drop procedure if exists [P_update_question]
--	Updates a question
go 
create procedure [P_update_question]
@userID int,
@questionID int,
@newDescr varchar(20) = null,
@newText varchar(255) = null,
@newType int = null
as
	declare @oldType int;
	update [question] set 
	[description] = COALESCE(@newDescr, [description]),
	[text] = COALESCE(@newText, [text]),
	[type] = COALESCE(@newType, [type])
	where [question_id] = @questionID
	exec sp_create_log @userID, 0, @questionID


--			Q6
go
drop procedure if exists [P_create_questionaire]
--	Create new empty questionaire
go
create procedure [P_create_questionaire]
@userID int,
@title varchar(20)
as
	insert into [questionaire](title, [version], isComplete, [creator_id], [previus_version])
	values(@title, 1, 0, @userID, null)

go
drop procedure if exists [P_clone_questionaire]
--	Clone completed questionaire
go
create procedure [P_clone_questionaire]
@companyID int,
@creatorID int,
@originalID int
as
if( (select [creator_id] from [questionaire] where [questionaire_id] = @originalID) in (select [id_number] from [user] where [company_id] = @companyID) )	-- Security check
begin
	declare  @originalQuestionaire table([questionaire_id] int, [title] varchar(20), [version] int, [isComplete] bit, [creator_id] int, [previus_version] int);
	insert into @originalQuestionaire	select*
										from [questionaire] q 
										where q.[questionaire_id] = @originalID;

	if( (select o.[isComplete] from @originalQuestionaire o) = 1)
	begin
		declare @title varchar(20) = (select o.title from @originalQuestionaire o);
		declare @version int = (select o.version from @originalQuestionaire o) + 1;
		declare @previous int = (select o.[questionaire_id] from @originalQuestionaire o);
		declare @newQuestionaireID table([id] int);

		while @version in (select q.[version] from [questionaire] q where q.[title] = @title)
			set @version = @version + 1

		begin transaction
			insert into [questionaire](title, [version], isComplete, [creator_id], [previus_version])
			output inserted.[questionaire_id] into @newQuestionaireID
			values (@title, @version, 0, @creatorID, @previous)
		
		if( @@ROWCOUNT = 0)
			rollback transaction


			insert into [questionaire_questions]([question_id], [questionaire_id])	
				select qq.[question_id] as [question_id], id.[id] as [questionaire_id]
				from [questionaire_questions] qq LEFT JOIN @newQuestionaireID id on (1=1)
				where qq.[questionaire_id] = @originalID
		if( @@ROWCOUNT = 0)
			rollback transaction
		else
			commit transaction
	end
	else
		print('Cannot clone unfinished questionaire');
end


go
drop procedure if exists [P_get_questionaires]
--	Returns questionaires from a company
go
create procedure [P_get_questionaires]
@companyID int,
@searchfortext varchar(255) = null	-- Search for title
as
select q.[questionaire_id], q.[title], q.[version], q.[isComplete], q.[creator_id], q.[previus_version]
from [questionaire] q
where q.[creator_id] in (select [id_number] from [user] where [company_id] = @companyID)
AND (@searchfortext is null OR q.[title] like CONCAT('%', @searchfortext, '%'))



go
drop procedure if exists [P_finalize_questionaire]
--	Set questionaire as complete
go
create procedure [P_finalize_questionaire]
@userID int,
@companyID int,
@questionaireID int
as
if( (select [creator_id] from [questionaire] where [questionaire_id] = @questionaireID) in (select [id_number] from [user] where [company_id] = @companyID) ) -- Security check
begin
	update [questionaire] set
	[isComplete] = 1
	where [questionaire_id] = @questionaireID
	exec sp_create_log @userID, 1, @questionaireID
end

go
drop procedure if exists [P_get_questionaire_questions]
--	Get questions of a questionaire
go
create procedure [P_get_questionaire_questions]
@companyID int,
@questionaireID int
as
if( (select [creator_id] from [questionaire] where [questionaire_id] = @questionaireID) in (select [id_number] from [user] where [company_id] = @companyID) ) -- Security check
	select qq.[question_id], q.[description], [dbo].[F_get_type_string](q.[type]) as type
	from [questionaire_questions] qq inner join [question] q on q.[question_id] = qq.[question_id]
	where qq.[questionaire_id] = @questionaireID


go
drop procedure if exists [P_add_question]
--	Add question to a questionaire
go
create procedure [P_add_question]
@userID int,
@companyID int,
@questionaireID int,
@questionID int
as
if( (select [creator_id] from [questionaire] where [questionaire_id] = @questionaireID) in (select [id_number] from [user] where [company_id] = @companyID) ) -- Security check
begin
	insert into [questionaire_questions]([questionaire_id], [question_id])
	values(@questionaireID, @questionID)
	exec sp_create_log @userID, 1, @questionaireID
end


go
drop procedure if exists [P_remove_question]
--	Removes a question from a questionaire
go
create procedure [P_remove_question]
@userID int,
@companyID int,
@questionaireID int,
@questionID int
as
if( (select [creator_id] from [questionaire] where [questionaire_id] = @questionaireID) in (select [id_number] from [user] where [company_id] = @companyID) ) -- Security check
begin
	delete from [questionaire_questions]
	where [question_id] = @questionID AND [questionaire_id] = @questionaireID
	exec sp_create_log @userID, 1, @questionaireID
end


--		Q7
go

drop procedure if exists sp_questions_of_user
go
--	Returns the questionaires of a user sorted by number of questions
create procedure sp_questions_of_user 
@company_id int
as
	select count(qq.[question_id]) as [Number_of_Questions], qq.questionaire_id
	from [questionaire_questions] qq join fu_company_questionare(@company_id) cq on qq.questionaire_id=cq.questionaire_id
	group by qq.questionaire_id
	order by [Number_of_Questions]
	
	

--	Q8
go

drop procedure if exists sp_popular_question
go
--	Returns most used questions
create procedure sp_popular_question
@company_id int
as
	select qp.[question_id]
	from fu_question_popularity(@company_id) qp
	where qp.questionaire_count >= all (	
		select qp1.questionaire_count
		from fu_question_popularity(@company_id) qp1
	)


--	Q9
go
drop procedure if exists sp_question_count_in_questionare
--	Returns the number of questions in each questionaire of a company
go
create procedure sp_question_count_in_questionare 
@company_id int = null
as
	set @company_id = null;

	select count(qq.[question_id]) as [Number_of_Questions], qq.questionaire_id
	from [questionaire_questions] qq, fu_company_questionare(@company_id) cq
	where qq.questionaire_id = cq.questionaire_id
	group by qq.questionaire_id 



 --		Q10


go
drop procedure if exists sp_question_average_in_questionare
--	Returs the average number of questions per questionaire of a company
go
create procedure sp_question_average_in_questionare
@company_id int
as
	select avg(cast(qcq.question_count as float)) as question_average
	from fu_question_count_in_questionare(@company_id) qcq


--		Q11


go
drop procedure if exists sp_questionares_more_avg_questions
--	Returns questionaires with more quesitions then the average of the company
go
create procedure sp_questionares_more_avg_questions 
@company_id int
as
	select qcq.questionaire_id
	from fu_question_average_in_questionare(@company_id) qaq, fu_question_count_in_questionare(@company_id) qcq
	where qcq.question_count>qaq.question_average

--		Q12


go
drop procedure if exists sp_questionares_least_questions
--	Returns the questionaires with the smallest amount of questions of a company
go
create procedure sp_questionares_least_questions 
@company_id int
as
	select qcq.questionaire_id
	from fu_question_count_in_questionare(@company_id) qcq
	where qcq.question_count <= all (
		select qcq1.question_count
		from fu_question_count_in_questionare(@company_id) qcq1
	)
go




--		Q13

go
if OBJECT_ID('P_find_same_questionaire','P') is not null
	drop procedure [P_find_same_questionaire]
-- Returns questionaires with the same questions from the same company
go
create procedure [P_find_same_questionaire]
@questionaireID int
as

	select q.[questionaire_id]
	from [questionaire] q
	where q.[questionaire_id] != @questionaireID AND q.[isComplete] = 1 AND not exists(
		
		select [qq1].[question_id]
		from [questionaire_questions] [qq1]
		where [qq1].questionaire_id = q.[questionaire_id]
		EXCEPT
		select [qq2].[question_id]
		from [questionaire_questions] [qq2]
		where [qq2].questionaire_id = @questionaireID
	) AND not exists(
		select [qq2].[question_id]
		from [questionaire_questions] [qq2]
		where [qq2].questionaire_id = @questionaireID
		EXCEPT
		select [qq1].[question_id]
		from [questionaire_questions] [qq1]
		where [qq1].questionaire_id = q.[questionaire_id]

	)
	order by q.[questionaire_id]


--		Q14
go
if OBJECT_ID('P_find_similar_questionaire','P') is not null
	drop procedure [P_find_similar_questionaire]
--	Retuns questoinaires that contain the questions of a questionaire
go
create procedure P_find_similar_questionaire
@questionaireID int
as

	select q.[questionaire_id]
	from [questionaire] q
	where q.[questionaire_id] != @questionaireID AND q.[isComplete] = 1 AND not exists(
		select [qq2].[question_id]
		from [questionaire_questions] [qq2]
		where [qq2].questionaire_id = @questionaireID
		EXCEPT
		select [qq1].[question_id]
		from [questionaire_questions] [qq1]
		where [qq1].questionaire_id = q.[questionaire_id]

	)
	order by q.[questionaire_id]

--		Q15
go
if OBJECT_ID('P_get_least_used_questions','P') is not null
	drop procedure [P_get_least_used_questions]
-- Retuns the least used questions
go
create procedure [P_get_least_used_questions]
@numberofQuestions int
as

	select top (@numberofQuestions) q.[question_id]
	from [question] as q LEFT JOIN [questionaire_questions] as qq on q.[question_id] = qq.[question_id]
	JOIN [questionaire] qe on qq.[questionaire_id] = qe.[questionaire_id]
	where qe.[isComplete] = 1
	group by q.[question_id]
	order by (
		select sum( case when qq.questionaire_id is null then 0 else 1 end)
	) asc

--		Q16
go
if OBJECT_ID('P_question_used_by_all','P') is not null
	drop procedure [P_question_used_by_all]
-- Returns the questions that are used by all questionaires of this company
go
create procedure [P_question_used_by_all]
@companyID int
as

	select q.question_id
	from [question] q join [user] u on q.user_id = u.id_number
	where u.company_id = @companyID 
	AND not exists(	-- Check if all complete questionaires use this question
		select qu.questionaire_id
		from questionaire qu inner join [user] u on qu.creator_id = u.id_number
		where u.company_id = @companyID AND qu.[isComplete] = 1
		EXCEPT
		select qu.questionaire_id
		from questionaire qu JOIN questionaire_questions qq on qu.questionaire_id = qq.questionaire_id
		where qq.question_id = q.question_id
	)	AND exists(	-- Check if this questions is used by at least 1 compl
		select *
		from [questionaire_questions] qq join [questionaire] qe on qq.[questionaire_id] = qe.[questionaire_id]
		where q.[question_id] = qq.[question_id] AND qe.[isComplete] = 1
	)

--		Q17



go
drop procedure if exists sp_questionares_inheritance_versions
go


create procedure sp_questionares_inheritance_versions --Q17
@questionare_id int
as
with cte as (
select q.questionaire_id, qcq.question_count
from fu_question_count_in_questionare(null) qcq join questionaire q on q.questionaire_id = qcq.questionaire_id
where q.questionaire_id = @questionare_id ---and qcq.questionaire_id=@questionare_id
--group by qcq.questionaire_id
union all
select qnext.questionaire_id, qcq1.question_count+previus.question_count
from fu_question_count_in_questionare(null) qcq1 join questionaire qnext on qnext.questionaire_id = qcq1.questionaire_id
join cte previus
on qnext.previus_version=previus.questionaire_id
)
select *
from cte
go