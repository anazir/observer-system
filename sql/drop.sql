-- FK Constraints
alter table [questionaire_questions]
drop constraint if exists [fk_questionaire]

alter table [questionaire_questions]  
drop constraint if exists [fk_question]

alter table [questionaire]  
drop constraint if exists [fk_parent]

alter table [user] 
drop constraint if exists [fk_company]

alter table [multiple_choice] 
drop constraint if exists [fk_main_question_multiple_choice]

alter table [bounds] 
drop constraint if exists [fk_main_question_bounds]

alter table [questionaire] 
drop constraint if exists [fk_question_creator]

alter table [question] 
drop constraint if exists [fk_questionaire_creator]

alter table [log]
drop constraint if exists [fk_change_user]


-- Tables
drop table if exists [user]
drop table if exists [company]
drop table if exists [question]
drop table if exists [multiple_choice]
drop table if exists [bounds]
drop table if exists [questionaire]
drop table if exists [questionaire_questions]
drop table if exists [log]


-- Triggers
drop trigger if exists [TR_question_del_upd]
drop trigger if exists [TR_bounds_modify_check]
drop trigger if exists [TR_multich_modify_check]
drop trigger if exists [TR_questionaire_upd]


-- Functions
drop function if exists fu_company_questionare
drop function if exists [F_get_type_string]
drop function if exists fu_question_popularity
drop function if exists fu_question_count_in_questionare
drop function if exists fu_question_average_in_questionare


-- Procedures
DROP PROCEDURE if exists [dbo].[sp_create_log];
DROP PROCEDURE if exists [dbo].[P_create_company];
DROP PROCEDURE if exists [dbo].[P_get_company_info];
DROP PROCEDURE if exists [dbo].[P_get_company_manager];
DROP PROCEDURE if exists [dbo].[P_modify_company];
DROP PROCEDURE if exists [dbo].[P_modify_manager];
DROP PROCEDURE if exists [dbo].[P_new_simple_user];
DROP PROCEDURE if exists [dbo].[P_get_simple_user_info];
DROP PROCEDURE if exists [dbo].[P_modify_simple_user];
DROP PROCEDURE if exists [dbo].[P_get_question_info];
DROP PROCEDURE if exists [dbo].[P_get_choices];
DROP PROCEDURE if exists [dbo].[P_create_question];
DROP PROCEDURE if exists [dbo].[P_delete_question];
DROP PROCEDURE if exists [dbo].[P_add_bounds];
DROP PROCEDURE if exists [dbo].[P_modify_bounds];
DROP PROCEDURE if exists [dbo].[P_remove_bounds];
DROP PROCEDURE if exists [dbo].[P_add_choice];
DROP PROCEDURE if exists [dbo].[P_modify_choice];
DROP PROCEDURE if exists [dbo].[P_remove_choice];
DROP PROCEDURE if exists [dbo].[P_update_question];
DROP PROCEDURE if exists [dbo].[P_create_questionaire];
DROP PROCEDURE if exists [dbo].[P_clone_questionaire];
DROP PROCEDURE if exists [dbo].[P_get_questionaires];
DROP PROCEDURE if exists [dbo].[P_finalize_questionaire];
DROP PROCEDURE if exists [dbo].[P_get_questionaire_questions];
DROP PROCEDURE if exists [dbo].[P_add_question];
DROP PROCEDURE if exists [dbo].[P_remove_question];
DROP PROCEDURE if exists [dbo].[sp_questions_of_user];
DROP PROCEDURE if exists [dbo].[sp_popular_question];
DROP PROCEDURE if exists [dbo].[sp_question_count_in_questionare];
DROP PROCEDURE if exists [dbo].[sp_question_average_in_questionare];
DROP PROCEDURE if exists [dbo].[sp_questionares_more_avg_questions];
DROP PROCEDURE if exists [dbo].[sp_questionares_least_questions];
DROP PROCEDURE if exists [dbo].[P_find_same_questionaire];
DROP PROCEDURE if exists [dbo].[P_find_similar_questionaire];
DROP PROCEDURE if exists [dbo].[P_get_least_used_questions];
DROP PROCEDURE if exists [dbo].[P_question_used_by_all];
DROP PROCEDURE if exists [dbo].[sp_questionares_inheritance_versions];
