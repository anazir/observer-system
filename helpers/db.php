<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/helpers/dotenv.php');

(new DotEnv(__DIR__ . '/../.env'))->load();

$dbHost = getenv('DB_HOST');
$dbName = getenv('DB_NAME');
$dbPass = getenv('DB_PASSWORD');

$conn = sqlsrv_connect($dbHost, array('Database' => $dbName, 'UID' => $dbName, 'PWD' => $dbPass));

if( $conn === false ) {
  die( print_r( sqlsrv_errors(), true));
}

?>

