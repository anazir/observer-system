<?php
session_start();

function forbid($userType) {
  $userEntity = ['simple-user', 'company-admin', 'observer-admin'];
  $curUserType = $_SESSION['user_type'];

  if ($curUserType == $userType)
    header("Location: http://www.cs.ucy.ac.cy/~anazir01/projects/observer-system/".$userEntity[$curUserType]."/");
}

function authenticate($userType) {
  $userEntity = ['simple-user', 'company-admin', 'observer-admin'];
  $curUserType = $_SESSION['user_type'];

  if (!isset($curUserType))
    header('Location: index.php');

  if (
    $curUserType != $userType && $curUserType == 0 ||
    $curUserType == 1 && $userType == 2
  )
    header("Location: http://www.cs.ucy.ac.cy/~anazir01/projects/observer-system/".$userEntity[$curUserType]."/");
  /* echo $curUserType; */
  /* echo $userType; */
}

function PrintResultSet ($resultSet) {
  if(!$resultSet) {
    echo print_r( sqlsrv_errors(), true );
    return;
  }

  echo ("<table class='table is-fullwidth'><tr >");

  foreach( sqlsrv_field_metadata($resultSet) as $fieldMetadata ) {
    echo ("<th class='is-uppercase'>");
    echo $fieldMetadata["Name"];
    echo ("</th>");
  }
  echo ("</tr>");

  while ($row = sqlsrv_fetch_array($resultSet, SQLSRV_FETCH_ASSOC)) {
    echo ("<tr>");
    foreach($row as $col){
      echo ("<td>");
      if(is_a($col, 'DateTime')) {
        $col = $col->format('Y-m-d');
      }
      echo (is_null($col) ? "Null" : $col);
      echo ("</td>");
    }
    echo ("</tr>");
  }
  echo ("</table>");
}

function PrintMultiResultSet($query) {
  if(!$query) {
    echo print_r( sqlsrv_errors(), true );
    return;
  }

  PrintResultSet($query);

  while ($next_result = sqlsrv_next_result($query)) {
    PrintResultSet($query);
  }
}

function myEmpty($val) {
  return empty($val) && $val != 0;
}
?>
