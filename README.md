# Observer System

## A web app created for CS342 as a project.
## A questionaire management system built with php and tSQL

This project got a score of 10/10.
Here's a link to the working system:
[https://www.cs.ucy.ac.cy/~anazir01/projects/observer-system/index.php](https://www.cs.ucy.ac.cy/~anazir01/projects/observer-system/index.php)

You'll need to create a database with the same name as the user of the database.
Then you'll need to run the files in the sql folder.
Also you'll need to create a file named .env with the following contents:

```
DB_HOST=
DB_NAME=
DB_PASSWORD=
```
